package slack;

import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import slack.model.SlackChannel;
import slack.model.SlackMessage;
import slack.reply.SlackChannelReply;
import slack.reply.SlackMessageReply;
import slack.service.SlackService;

import static org.junit.Assert.*;

@SpringBootTest(classes = SlackApp.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@RunWith(SpringRunner.class)
public class SlackServiceIntegrationTest {

    private static SlackChannel privateChannel;

    private static SlackChannel publicChannel;

    @Autowired
    private SlackService service;

    @BeforeClass
    public static void setUp() {
        privateChannel = new SlackChannel("chahnnel_pr", true);
        publicChannel = new SlackChannel("channel_pub", false);
    }

    @Test
    public void whenChannelNameIsNotProvided_whenRequestIsExecuted_thenErrorIsReceived() {

        // Given
        SlackChannel channel = new SlackChannel();
        channel.setPrivate(false);

        // When
        SlackChannelReply result = service.openConversation(channel);

        // Then
        assertFalse(result.isOk());
    }

    @Test
    public void whenChannelNameIsNotProvided_whenRequestIsExecuted_thenErrorListIsNotEmpty() {

        // Given
        SlackChannel channel = new SlackChannel();
        channel.setPrivate(true);

        // When
        service.openConversation(channel);

        // Then
        assertTrue(!SlackService.getErrorsList().isEmpty());
    }

    @Test
    public void a_GivenPublicChannel_whenRequestIsExecuted_thenConversationIsOpened() {

        // Given publicChannel

        // When
        SlackChannelReply result = service.openConversation(publicChannel);
        publicChannel.setChannel(result.getChannel().getId());

        // Then
        assertTrue(result.isOk());
    }

    @Test
    public void b_GivenPrivateChannel_whenRequestIsExecuted_thenConversationIsOpened() {

        // Given privateChannel

        // When
        SlackChannelReply result = service.openConversation(privateChannel);
        privateChannel.setChannel(result.getChannel().getId());

        // Then
        assertTrue(result.isOk());
    }

    @Test
    public void w_givenPublicChannel_whenRequestIsExecuted_thenMessageIsSent() {

        // Given
        SlackMessage message = new SlackMessage(publicChannel.getName(), "hello");

        // When
        SlackMessageReply result = service.sendMessage(message);

        // Then
        assertTrue(result.isOk());
    }

    @Test
    public void y_givenPrivateChannel_whenRequestIsExecuted_thenMessageIsSent() {

        // Given
        SlackMessage message = new SlackMessage(privateChannel.getName(), "hello");

        // When
        SlackMessageReply result = service.sendMessage(message);

        // Then
        assertTrue(result.isOk());
    }

    @Test
    public void x_GivenPublicChannel_whenRequestIsExecuted_thenChannelIsArchived() {

        // Given publicChannel

        // When
        SlackChannelReply result = service.archiveChannel(publicChannel);

        // Then
        assertTrue(result.isOk());
    }

    @Test
    public void z_GivenPrivateChannel_whenRequestIsExecuted_thenChannelIsArchived() {

        // Given privateChannel

        // When
        SlackChannelReply result = service.archiveChannel(privateChannel);

        // Then
        assertTrue(result.isOk());
    }

    @Test
    public void givenPublicChannel_whenSlackMessageIsSent_thenMessageIsRecieved() {

        // Given
        SlackMessage message = new SlackMessage(publicChannel.getName(), "hello");

        // When
        SlackMessageReply result = service.sendMessage(message);

        // Then
        assertThat("hello", Matchers.is(result.getMessage().getText()));
    }

    @Test
    public void givenPrivateChannel_whenSlackMessageIsSent_thenMessageIsRecieved() {

        // Given
        SlackMessage message = new SlackMessage(privateChannel.getName(), "hello");

        // When
        SlackMessageReply result = service.sendMessage(message);

        // Then
        assertThat("hello", Matchers.is(result.getMessage().getText()));
    }

}
