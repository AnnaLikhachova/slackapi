package slack.service;

import com.slack.api.app_backend.interactive_components.payload.MessageShortcutPayload;
import com.slack.api.methods.SlackApiException;
import com.slack.api.methods.response.channels.ChannelsArchiveResponse;
import com.slack.api.methods.response.chat.ChatPostMessageResponse;
import com.slack.api.methods.response.conversations.ConversationsCreateResponse;
import com.slack.api.model.event.MessageEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.slack.api.bolt.App;
import com.slack.api.bolt.AppConfig;
import com.slack.api.bolt.jetty.SlackAppServer;

import java.io.IOException;

@Service
public class LambdaSlackService {
/*
    private static final Logger logger = LoggerFactory.getLogger(LambdaSlackService.class);

    @Value("${slack.verification.token}")
    private String SLACK_TOCKEN;

    @Value("${slack.secret}")
    private String SLACK_SIGNING_SECRET;

    public void createConversation() {
        AppConfig config = new AppConfig();
        config.setSingleTeamBotToken(SLACK_TOCKEN);
        config.setSigningSecret(System.getenv(SLACK_SIGNING_SECRET));
        App app = new App(config); // `new App()` does the same

        // Listen for a message shortcut with the callback_id "sample_message_action"
        // Message shortcuts require the commands scope
        app.messageShortcut("sample_message_action", (req, ctx) -> {

            try {
                MessageShortcutPayload payload = req.getPayload();
                // Call the conversations.create method using the built-in WebClient
                ConversationsCreateResponse result = ctx.client().conversationsCreate(r -> r
                        // The token you used to initialize your app
                        .token(SLACK_TOCKEN)
                        // The name of the conversation
                        .name("pretend-channel")
                );
                // Print result
                logger.info("result: {}", result);
            }
            catch(IOException | SlackApiException e) {
                logger.error("error: {}", e.getMessage(), e);
            }
            // Acknowledge incoming command event
            return ctx.ack();
        });

        SlackAppServer server = new SlackAppServer(app);
        try {
            server.start();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void sendMessage() {
        AppConfig config = new AppConfig();
        config.setSingleTeamBotToken(SLACK_TOCKEN);
        config.setSigningSecret(System.getenv(SLACK_SIGNING_SECRET));
        App app = new App(config); // `new App()` does the same

        app.message("hello", (req, ctx) -> {
            try {
                MessageEvent event = req.getEvent();
                // Call the chat.postMessage method using the built-in WebClient
                ChatPostMessageResponse result = ctx.client().chatPostMessage(r -> r
                        // The token you used to initialize your app is stored in the `context` object
                        .token(ctx.getBotToken())
                        // Payload message should be posted in the channel where original message was heard
                        .channel(event.getChannel())
                        .text("world")
                );
                logger.info("result: {}", result);
            }
            catch(IOException | SlackApiException e) {
                logger.error("error: {}", e.getMessage(), e);
            }
            return ctx.ack();
        });

        SlackAppServer server = new SlackAppServer(app);
        try {
            server.start();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }
*/
}







