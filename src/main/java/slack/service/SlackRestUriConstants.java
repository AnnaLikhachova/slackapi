package slack.service;

public class SlackRestUriConstants {
    public static final String CREATE_CONVERSATION = "https://slack.com/api/conversations.create";
    public static final String SEND_MESSAGE = "https://slack.com/api/chat.postMessage";
    public static final String ARCHIVE_CHANNEL = "https://slack.com/api/conversations.archive";
}
