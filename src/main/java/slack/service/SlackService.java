package slack.service;

import com.sun.jmx.snmp.Timestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import slack.model.SlackChannel;
import slack.model.SlackMessage;
import slack.reply.SlackChannelReply;
import slack.reply.SlackMessageReply;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class SlackService {

    private static final Logger logger = LoggerFactory.getLogger(SlackService.class);

    private static final HttpHeaders headers = new HttpHeaders();

    private static final Map<String, SlackChannel> errorsList = new ConcurrentHashMap<String, SlackChannel>();

    @Value("${slack.verification.token}")
    private String SLACK_TOCKEN;

    @Autowired
    private RestTemplate restTemplate;

   public SlackService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @PostConstruct
    private void addHeaders() {
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", SLACK_TOCKEN);
    }

    public SlackChannelReply openConversation(SlackChannel channel) {
        logger.info("Start openConversation");
        HttpEntity<SlackChannel> entity = new HttpEntity<>(channel, headers);
        ResponseEntity<SlackChannelReply> response = restTemplate.postForEntity(SlackRestUriConstants.CREATE_CONVERSATION, entity, SlackChannelReply.class);
        return processChannelResponse(response, channel, entity, SlackRestUriConstants.CREATE_CONVERSATION, "OPEN CONVERSATION");
    }

    public SlackChannelReply archiveChannel(SlackChannel channel) {
        logger.info("Start archiveChannel");
        HttpEntity<SlackChannel> entity = new HttpEntity<>(channel, headers);
        ResponseEntity<SlackChannelReply> response = this.restTemplate.postForEntity(SlackRestUriConstants.ARCHIVE_CHANNEL, entity, SlackChannelReply.class);
        return processChannelResponse(response, channel, entity, SlackRestUriConstants.ARCHIVE_CHANNEL, "ARCHIVE CHANNEL");
    }

    public SlackMessageReply sendMessage(SlackMessage message) {
        logger.info("Start sendMessage");
        HttpEntity<SlackMessage> entity = new HttpEntity<>(message, headers);
        ResponseEntity<SlackMessageReply> response = this.restTemplate.postForEntity(SlackRestUriConstants.SEND_MESSAGE, entity, SlackMessageReply.class);
        return processMessageResponse(response, entity, SlackRestUriConstants.SEND_MESSAGE);
    }

    private SlackChannelReply processChannelResponse(ResponseEntity<SlackChannelReply> response, SlackChannel channel, HttpEntity<SlackChannel> entity, String url, String action) {
        SlackChannelReply slackChannelReply = response.getBody();
        if(response.getStatusCode() == HttpStatus.OK && slackChannelReply != null) {
            if(slackChannelReply.isOk()) {
                logger.info("The channel " + channel.getName() + " status Ok " + action);
            }
            else {
                ResponseEntity<SlackChannelReply> responseSecond = this.restTemplate.postForEntity(url, entity, SlackChannelReply.class);
                slackChannelReply = response.getBody();
                if(responseSecond.getStatusCode() == HttpStatus.OK && slackChannelReply != null) {
                    if(slackChannelReply.isOk()) {
                        logger.info("The channel " + channel.getName() + " status Ok " + action);
                    }
                    else {
                        logger.info("The channel " + channel.getName() + " status Error " + action);
                        errorsList.put(slackChannelReply.getErrorMessage() + " time: " + new Timestamp(), channel);
                    }
                }
            }
        }
        return slackChannelReply;
    }

    private SlackMessageReply processMessageResponse(ResponseEntity<SlackMessageReply> response, HttpEntity<SlackMessage> entity, String url) {
        SlackMessageReply slackMessageReply = response.getBody();
        if(response.getStatusCode() == HttpStatus.OK && slackMessageReply != null) {
            if(slackMessageReply.isOk()) {
                logger.info("The message was posted in channel " + slackMessageReply.getMessage().getText());
            }
            else {
                ResponseEntity<SlackMessageReply> responseSecond = this.restTemplate.postForEntity(url, entity, SlackMessageReply.class);
                slackMessageReply = responseSecond.getBody();
                if(responseSecond.getStatusCode() == HttpStatus.OK && slackMessageReply != null) {
                    if(slackMessageReply.isOk()) {
                        logger.info("The message was posted in channel " + slackMessageReply.getMessage().getText());
                    }
                    else {
                        logger.info("The message was NOT posted in channel  " + slackMessageReply.getChannel() + " status" + slackMessageReply.getErrorMessage());
                        errorsList.put(slackMessageReply.getErrorMessage() + " time: " + new Timestamp(), new SlackChannel(slackMessageReply.getChannel()));
                    }
                }
            }
        }
        return slackMessageReply;
    }

    public static Map<String, SlackChannel> getErrorsList() {
        return errorsList;
    }
}
