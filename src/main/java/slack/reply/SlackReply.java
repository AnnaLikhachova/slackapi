package slack.reply;

public interface SlackReply {
    boolean isOk();
    String getErrorMessage();
}
