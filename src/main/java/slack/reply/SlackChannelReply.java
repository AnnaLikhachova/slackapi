package slack.reply;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import slack.model.SlackChannel;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SlackChannelReply implements SlackReply {
    private boolean ok;
    private String error;
    private SlackChannel channel;
    @Override
    public boolean isOk() {
        return ok;
    }

    @Override
    public String getErrorMessage() {
        return error;
    }
}
