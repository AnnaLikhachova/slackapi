package slack.reply;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import slack.model.SlackMessage;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SlackMessageReply implements SlackReply{

    private boolean ok;
    private String error;
    private SlackMessage message;
    private String channel;

    @Override
    public boolean isOk()
    {
        return ok;
    }

    @Override
    public String getErrorMessage()
    {
        return error;
    }


}
