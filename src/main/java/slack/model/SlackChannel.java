package slack.model;

import lombok.*;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class SlackChannel {
    private String id;
    private String channel;
    private String name;
    private boolean isPrivate;

    public SlackChannel(String name, boolean isPrivate) {
        this.name = name;
        this.isPrivate = isPrivate;
    }

    public SlackChannel(String channel) {
        this.channel = channel;
    }
}
