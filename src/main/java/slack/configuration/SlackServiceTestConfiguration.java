package slack.configuration;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import slack.service.SlackService;

//@Profile("test")
//@Configuration
public class SlackServiceTestConfiguration {

    //@Bean
   // @Primary
    public SlackService slackService() {
        return Mockito.mock(SlackService.class);
    }


}
